package imp.jfx

import javafx.scene.Node
import javafx.scene.control.ListCell


/**ListCell where you don't have to implement updateItem().
 * Just provide a Node and make sure it's updated based on changes to this.itemProperty().*/
abstract class ImpListCell <T> : ListCell<T>() {
	protected abstract val view: Node

	final override fun updateItem(item: T?, empty: Boolean) {
		super.updateItem(item, empty)
		this.text = null
		this.graphic = if(empty) null else view
	}
}