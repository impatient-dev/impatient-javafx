package imp.jfx

import javafx.collections.ListChangeListener
import javafx.collections.ObservableList

class EmptyListChangeListener <T> (list: ObservableList<T>) : ListChangeListener.Change<T>(list) {
	override fun next() = false

	override fun reset() = throw UnsupportedOperationException()
	override fun getFrom() = throw UnsupportedOperationException()
	override fun getTo() = throw UnsupportedOperationException()
	override fun getRemoved() = throw UnsupportedOperationException()
	override fun getPermutation() = throw UnsupportedOperationException()

	override fun wasAdded() = false
	override fun wasPermutated() = false
	override fun wasRemoved() = false
	override fun wasReplaced() = false
	override fun wasUpdated() = false
}