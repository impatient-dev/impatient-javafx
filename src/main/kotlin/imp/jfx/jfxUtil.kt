package imp.jfx

import imp.util.logger
import javafx.application.Platform
import javafx.beans.Observable
import javafx.beans.binding.Bindings
import javafx.beans.property.BooleanProperty
import javafx.beans.property.Property
import javafx.beans.property.ReadOnlyObjectWrapper
import javafx.beans.property.ReadOnlyProperty
import javafx.beans.value.ChangeListener
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.util.Callback
import javafx.util.Duration
import java.util.concurrent.Callable
import kotlin.reflect.KClass

//helper functions for working with JavaFX


/**If we're on the JavaFX main thread, runs this now. Otherwise, runs it later on the JavaFX main thread.*/
fun jfxNowOrLater(runnable: Runnable) {
	if(Platform.isFxApplicationThread())
		runnable.run()
	else
		Platform.runLater(runnable)
}


// STYLES

/**Adds a classpath resource (some file in src/main/resources, e.g. "/style.css") as a stylesheet.*/
fun <T: Any> Parent.addStylesheetResource(name: String, someAppClass: KClass<T>) {
	this.stylesheets.add(someAppClass.java.getResource(name).toExternalForm())
}

fun <T: Labeled> T.withText(str: String): T {
	this.text = str
	return this
}

/**Removes all style classes from the node.*/
fun <T: Node> T.withNoStyle(): T {
	this.styleClass.clear()
	return this
}
/**Adds a style class to a node. */
fun <T: Node> T.withStyle(cls: String): T {
	this.styleClass.add(cls)
	return this
}
/**Adds style classes to a node. */
fun <T : Node> T.withStyle(vararg classes: String): T {
	this.styleClass.addAll(*classes)
	return this
}

/**Adds a style class, where the name of the enum is the class.*/
fun <T: Node> T.withStyle(style: Enum<*>) = this.withStyle(style.name)

/**Removes a style class from a node. */
fun <T: Node> T.withoutStyle(cls: String): T {
	this.styleClass.remove(cls)
	return this
}

/**Removes a style class, where the name of the enum is the class.*/
fun <T: Node> T.withoutStyle(style: Enum<*>) = this.withoutStyle(style.name)

/**Adds or removes a style class, depending on the boolean.*/
fun <T : Node> T.withStyle(cls: String, present: Boolean): T {
	if(present)
		this.styleClass.add(cls)
	else
		this.styleClass.remove(cls)
	return this
}
/**Adds or removes a style class, depending on the boolean.*/
fun <T: Node> T.withStyle(cls: Enum<*>, present: Boolean): T = this.withStyle(cls.name, present)



// WITH

fun <T: ButtonBase> T.withAction(action: EventHandler<ActionEvent>): T {
	this.onAction = action
	return this
}

fun <T: HBox> T.withAlign(align: Pos): T {
	this.alignment = align
	return this
}
fun <T: VBox> T.withAlign(align: Pos): T {
	this.alignment = align
	return this
}

/**Marks this button as a cancel button, triggered by the escape key.*/
fun <T: Button> T.withCancelButton(): T{
	this.isCancelButton = true
	return this
}

/**Marks this button as a default button, triggered by the enter key.*/
fun <T: Button> T.withDefaultButton(): T{
	this.isDefaultButton = true
	return this
}

fun <T: Control> T.withTooltip(str: String): T {
	val tooltip = Tooltip(str)
	tooltip.showDelay = Duration.ZERO
	tooltip.hideDelay = Duration.ZERO
	this.tooltip = tooltip
	return this
}

fun <T: Labeled> T.withWrap(wrap: Boolean): T {
	this.isWrapText = true
	return this
}

fun <T> ListView<T>.withCellFactory(factory: Callback<ListView<T>, ListCell<T>>): ListView<T> {
	this.cellFactory = factory
	return this
}

fun <T: MenuItem> T.withAction(action: EventHandler<ActionEvent>): T {
	this.onAction = action
	return this
}

fun <T: TextInputControl> T.withPrompt(str: String): T {
	this.promptText = str
	return this
}


// PROPERTIES

/**Call this when you want to set a property but aren't on the JavaFX application thread.*/
fun <T> Property<T>.setLater(value: T) {
	Platform.runLater {this.value = value}
}

fun BooleanProperty.toggle() {
	this.value = !this.value
}

/**Binds a destination property to a source property so the destination will copy the source's value.*/
fun <T> Property<T>.jfxBind(srcProperty: ReadOnlyProperty<T>) {
	this.bind(Bindings.createObjectBinding(Callable { srcProperty.value }, srcProperty))
}
/**Binds a destination property to a source property so the destination value will be transform(src.get()).*/
fun <Src, Dest> Property<Dest>.jfxBind(srcProperty: ReadOnlyProperty<Src>, transform: (Src) -> Dest) {
	this.bind(Bindings.createObjectBinding(Callable { transform(srcProperty.value) }, srcProperty))
}
/**Binds a destination property to 1 or more source properties, so the value will be updated by the callable every time any of the sources changes.*/
fun <Dest> Property<Dest>.jfxBind(vararg sources: Observable, callable: Callable<Dest>) {
	this.bind(Bindings.createObjectBinding(callable, *sources))
}

/**Creates a change listener that will be called with the new value of this property every time it changes, and calls the listener immediately with the current value.*/
fun <T> Property<T>.addListenerAndCallImmediately(block: (T) -> Unit) {
	this.addListener(ChangeListener {_, _, value -> block(value)})
	block(this.value)
}

fun <T> Property<T>.addListenerAndCallImmediately(listener: ChangeListener<T>) {
	this.addListener(listener)
	listener.changed(this, this.value, this.value)
}


// OBSERVABLE COLLECTIONS

/**Registers the listener, then calls it with an argument representing 0 changes.*/
fun <T> ObservableList<T>.addListenerAndCallImmediately(listener: ListChangeListener<T>) {
	this.addListener(listener)
	listener.onChanged(EmptyListChangeListener(this))
}


// OTHER


fun <Row, Out> jfxTableCol(name: String, f: (Row) -> Out): TableColumn<Row, Out> {
	val out = TableColumn<Row, Out>(name)
	out.cellValueFactory = Callback { cdf: TableColumn.CellDataFeatures<Row, Out> -> ReadOnlyObjectWrapper(f(cdf.value)) }
	return out
}

/**Removes all children and adds the specified one.*/
fun StackPane.replaceChild(newChild: Node) {
	this.children.clear()
	this.children.add(newChild)
}
/**Removes all children and adds the specified one, unless the new child is null, in which case the pane will be left empty.*/
fun StackPane.replaceChildOpt(newChild: Node?) {
	this.children.clear()
	if(newChild != null)
		this.children.add(newChild)
}


object JfxUtil {
	private val log = JfxUtil::class.logger

	/**Loads a JavaFX image by its path, using any class from the module that contains the resource.
	 * The path should be absolute, starting with "/".*/
	fun loadImage(cls: KClass<*>, path: String): Image {
		log.trace("Loading classpath JavaFX image {} from {}.", path, cls.java.name)
		return Image(cls.java.getResourceAsStream(path))
	}
}