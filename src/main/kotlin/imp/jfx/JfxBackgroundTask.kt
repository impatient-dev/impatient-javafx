package imp.jfx

import javafx.application.Platform

/**A Runnable that runs some functions in the JavaFX main thread after the main part of the task finishes.
 * success, fail, and finish all do nothing by default; you should set at least one of them to do something - otherwise there's no reason to use this class.*/
class JfxBackgroundTask <R> (
	/**Runs in the thread that's asked to execute this Runnable - presumably a background thread.*/
	private val background: () -> R,
	/**Runs in the JavaFX main thread if the background task succeeds, taking the result of the background part as its argument.*/
	private val success: (R) -> Unit = {},
	/**Runs in the JavaFX main thread if the background task fails by throwing something, taking the Throwable as its argument.*/
	private val fail: (Throwable) -> Unit = {},
	/**Runs in the JavaFX main thread after the background task runs, regardless of whether it succeeds or fails.*/
	private val finish: () -> Unit = {},
) : Runnable {
	override fun run() {
		try {
			val result = background()
			Platform.runLater {
				success(result)
				finish()
			}
		} catch(e: Throwable) {
			Platform.runLater {
				fail(e)
				finish()
			}
		}
	}
}