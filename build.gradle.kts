plugins {
	id("org.jetbrains.kotlin.jvm")
	id("org.openjfx.javafxplugin")
}

group = "imp-dev"
version = "0.0.0"
val jvm_version: String by project

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":impatient-utils"))
}

javafx {
	version = jvm_version
	modules("javafx.controls")
}